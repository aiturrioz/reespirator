
#ifndef _DEFAULTS_H
#define _DEFAULTS_H

#define PRUEBAS 1 // testing over arduino without sensors

/*
 * Constants
 */

// Timebase
#define TIME_COMMS                          100                // msec Telemetry/telecommand period to/from the HMI
#define TIME_SENSOR                         8                  // msec Sensor readings
#define TIME_BASE                           20                 // msec PID refresh
#define TIME_BASE_MICROS                    (TIME_BASE * 1000) // microsec PID refresh (for ISR)
#define TIME_STEPPER_ISR_MICROS             50                 // microsec Stepper refresh (for ISR)
#define TIME_SEND_CONFIGURATION             1000               // msec Ventilation configuration sent to the HMI

// Sensors
#define ENABLED_SENSOR_VOLUME               1
#if ENABLED_SENSOR_VOLUME
#define ENABLED_SENSOR_VOLUME_SFM3300       0
#define ENABLED_SENSOR_VOLUME_SDP800        1
#define ENABLED_PITOT_METER                 1
#define ENABLED_VENTURI_METER               0
#endif
#if ENABLED_PITOT_METER
#define K_PITOT_FLOW_LPM_PRESSURE_PA        2.9049F
#endif

// Stepper
#define STEPPER_MICROSTEPS                  4       // microsteps per step
#define STEPPER_STEPS_PER_REVOLUTION        200     // steps
#define STEPPER_DRIVER_INVERTED_LOGIC       true    /** @warning check your stepper driver specs! */

#define STEPPER_MICROSTEPS_PER_REVOLUTION   (STEPPER_STEPS_PER_REVOLUTION * STEPPER_MICROSTEPS)
#define STEPPER_DIR                         1
#define STEPPER_HOMING_DIRECTION            (-1)
#define STEPPER_HOMING_SPEED                (STEPPER_MICROSTEPS * 100)    // steps/s
#define STEPPER_LOWEST_POSITION             (STEPPER_MICROSTEPS *  85)    // steps
#define STEPPER_HIGHEST_POSITION            (STEPPER_MICROSTEPS * -100)   // steps
#define STEPPER_SPEED_DEFAULT               (STEPPER_MICROSTEPS *  800)   // steps/s
#define STEPPER_ACC_EXSUFFLATION            (STEPPER_MICROSTEPS *  600)   // steps/s2
#define STEPPER_ACC_INSUFFLATION            (STEPPER_MICROSTEPS *  450)   // steps/s2

// Defaults
#define DEFAULT_HEIGHT                      170   // cm
#define DEFAULT_SEX                         0     // 0: male, 1: female
#define DEFAULT_ML_PER_KG_IDEAL_WEIGHT      7     // ml per kg
#define DEFAULT_MAX_TIDAL_VOLUME            800   // ml
#define DEFAULT_MIN_TIDAL_VOLUME            240   // ml
#define DEFAULT_TRIGGER_THRESHOLD           3     // liters per minute
#define DEFAULT_RPM                         14    // breaths per minute
#define DEFAULT_MAX_RPM                     24    // breaths per minute
#define DEFAULT_MIN_RPM                     3     // breaths per minute
#define DEFAULT_INSPIRATORY_FRACTION        33    // % of cycle time
#define DEFAULT_PEAK_INSPIRATORY_PRESSURE   20    // cmH2O
#define DEFAULT_PEAK_ESPIRATORY_PRESSURE    10    // cmH2O
#define STEPPER_PEEP_SOLENOID_HYSTERESIS    0.5F  // cmH2O
#define STEPPER_VCV_FLOW_SOLENOID_HYSTERESIS 0.05 // liters per minute

// Pressure conversion rate
#define DEFAULT_PA_TO_CM_H2O                0.0102F

// Recruitment
#define DEFAULT_RECRUITMENT_TIMEOUT         40000 // msec
#define DEFAULT_RECRUITMENT_PIP             40    // cmH2O

// Alarms
#define ALARM_OVERPRESSURE                  58   // cmH2O
#define ALARM_MAX_PRESSURE_OVER_PIP         10   // cmH2O
#define ALARM_NO_PRESSURE                   1    // cmH2O
#define MAX_TIME_ALARM_MUTED                3000 // ms, timeout to alarm gets automatically unmuted again

// Overpressure that triggers valve
#define VALVE_MAX_PRESSURE                  58 // cmH2O

// PID constants
#define PID_MIN                             -20000 // steps per second
#define PID_MAX                             20000  // steps per second
#define PID_PRESSURE_KP                     80
#define PID_PRESSURE_KI                     2
#define PID_PRESSURE_KD                     5
#define PID_VOLUME_KP                       700
#define PID_VOLUME_KI                       40
#define PID_VOLUME_KD                       100
#define PID_TS                              TIME_BASE
#define PID_BANGBANG                        8

// Solenoid
#define SOLENOID_CLOSED                     0
#define SOLENOID_OPEN                       1


/*
 * Pinout
 */

#define PIN_STEPPER_STEP                    6
#define PIN_STEPPER_DIRECTION               7
#define PIN_STEPPER_EN                      8
#define PIN_STEPPER_ALARM                   3
#define PIN_BUZZ                            11
#define PIN_STEPPER_ENDSTOP                 2
#define PIN_SOLENOID                        39
#define PIN_RELAY                           25
#define PIN_POWER_BEAGLE                    30

#if ENABLED_SENSOR_VOLUME_SDP800
#define PIN_SDP800_A                        44
#define PIN_SDP800_B                        45
#endif

/*
 * Types
 */

/**
 * @brief Options that initializate ventilation.
 *
 * @warning height and sex are unused, as no initial tidal volume estimation is implemented.
 */
typedef struct {
    short height;
    bool sex;
    short respiratoryRate;
    short peakInspiratoryPressure;
    short peakEspiratoryPressure;
    float triggerThreshold;
    bool hasTrigger;
} VentilationOptions_t;

#endif // DEFAULTS_H
