/*
 *  Copyright (c) 2018, Sensirion AG <joahnnes.winkelmann@sensirion.com>
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *      * Redistributions of source code must retain the above copyright
 *        notice, this list of conditions and the following disclaimer.
 *      * Redistributions in binary form must reproduce the above copyright
 *        notice, this list of conditions and the following disclaimer in the
 *        documentation and/or other materials provided with the distribution.
 *      * Neither the name of the Sensirion AG nor the names of its
 *        contributors may be used to endorse or promote products derived
 *        from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 *  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 *  ARE DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *  THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <Arduino.h>
#include <Wire.h>
#include "sdpsensor.h"

bool SDPSensor::init()
{
  // try to read product id
  const uint8_t CMD_LEN = 2;
  uint8_t cmd0[CMD_LEN] = {0x36, 0x7C};
  uint8_t cmd1[CMD_LEN] = {0xE1, 0x02};

  const uint8_t DATA_LEN = 18;
  uint8_t data[DATA_LEN] = {0};
  uint8_t ret = 1;
  uint8_t i = 0;

  stopContMeasure();

  while (ret != 0)
  {
    ret = _i2cWrite(mI2CAddress, cmd0, CMD_LEN);
    ret = _i2cWrite(mI2CAddress, cmd1, CMD_LEN);
    ret = _i2cRead(mI2CAddress, data, DATA_LEN);
    ret = readSample();

    delay(10);

    if (i == 5)
    {
      return false;
    }
    Serial.print(F("Counter: "));
    Serial.println(i);
    i++;
  }
  // at this point, we don't really care about the data just yet, but
  // we may use that in the future. Either way, the sensor responds.
  return true;
}

int SDPSensor::readSample()
{
  const uint8_t CMD_LEN = 2;
  uint8_t cmd[CMD_LEN] = {0x36, 0x2F};

  const uint8_t DATA_LEN = 9;
  uint8_t data[DATA_LEN] = {0};

  if (_i2cWrite(mI2CAddress, cmd, CMD_LEN) != 0)
  {
    return 1;
  }

  delay(100); // theoretically 45ms

  if (_i2cRead(mI2CAddress, data, DATA_LEN) != 0)
  {
    return 1;
  }

  // TODO: check CRC

  int16_t dp_raw = (int16_t)data[0] << 8 | data[1];
  int16_t temp_raw = (int16_t)data[3] << 8 | data[4];
  int16_t dp_scale = (int16_t)data[6] << 8 | data[7];
  mScale = dp_scale;
  mDifferentialPressure = (float)dp_raw / dp_scale;
  mTemperature = temp_raw / 200.0;

  return 0;
}

float SDPSensor::getRapidPressure()
{
  const uint8_t CMD_LEN = 2;
  uint8_t cmd[CMD_LEN] = {0x36, 0x2F};

  const uint8_t DATA_LEN = 3;
  uint8_t data[DATA_LEN] = {0};

  if (_i2cWrite(mI2CAddress, cmd, CMD_LEN) != 0)
  {
    return 1;
  }

  delay(50); // theoretically 45ms

  if (_i2cRead(mI2CAddress, data, DATA_LEN) != 0)
  {
    return 1;
  }

  int16_t dp_raw = (int16_t)data[0] << 8 | data[1];
  mDifferentialPressure = (float)dp_raw / mScale;

  return mDifferentialPressure;
}

int SDPSensor::startContMeasure()
{
  const uint8_t CMD_LEN = 2;
  uint8_t cmd[CMD_LEN] = {0x36, 0x1E};
  if (_i2cWrite(mI2CAddress, cmd, CMD_LEN) != 0)
  {
    return 1;
  }
  return 0;
}

int SDPSensor::stopContMeasure()
{
  const uint8_t CMD_LEN = 2;
  uint8_t cmd[CMD_LEN] = {0x3F, 0xF9};
  if (_i2cWrite(mI2CAddress, cmd, CMD_LEN) != 0)
  {
    return 1;
  }
}

bool SDPSensor::reset()
{
  const uint8_t CMD_LEN = 2;
  uint8_t cmd[CMD_LEN] = {0x00, 0x06};
  if (_i2cWrite(mI2CAddress, cmd, CMD_LEN) != 0)
  {
    return true;
  }
  return false;
}

float SDPSensor::getContPressure()
{

  const uint8_t DATA_LEN = 3;
  uint8_t data[DATA_LEN] = {0};

  // Uncomment if this function is used once per 1 ms
  // delayMicroseconds(500);

  if (_i2cRead(mI2CAddress, data, DATA_LEN) != 0)
  {
    stopContMeasure();
    delayMicroseconds(100);
    startContMeasure();
    return 1000;
  }

  // TODO: check CRC
  
  int16_t dp_raw = (int16_t)data[0] << 8 | data[1];
  if (dp_raw == -16385)
  {
    stopContMeasure();
    delayMicroseconds(100);
    startContMeasure();
    return 2000;
  }
  mDiffbrut = dp_raw;
  mDifferentialPressure = (float)dp_raw / mScale;

  return mDifferentialPressure;
  //return mDiffbrut;
}

int SDPSensor::getDifferentialBrut() const
{
  return mDiffbrut;
}

float SDPSensor::getDifferentialPressure() const
{
  return mDifferentialPressure;
}

float SDPSensor::getTemperature() const
{
  return mTemperature;
}

/*
 * i2c methods
 */

int8_t SDPSensor::_i2cRead(uint8_t addr, uint8_t* data, uint16_t count)
{
    Wire.requestFrom(addr, count);
    if (Wire.available() != count) {
        return -1;
    }
    for (int i = 0; i < count; ++i) {
        data[i] = Wire.read();
    }
    return 0;
}

int8_t SDPSensor::_i2cWrite(uint8_t addr, const uint8_t* data, uint16_t count, bool appendCrc)
{
    Wire.beginTransmission(addr);
    for (int i = 0; i < count; ++i) {
        if (Wire.write(data[i]) != 1) {
            return 1;
        }
    }
    if (appendCrc) {
      uint8_t crc = _crc8(data, count);
      if (Wire.write(crc) != 1) {
          return 2;
      }
    }

    if (Wire.endTransmission() != 0) {
        return 3;
    }
    return 0;
}

uint8_t SDPSensor::_crc8(const uint8_t* data, uint8_t len)
{
  // adapted from SHT21 sample code from http://www.sensirion.com/en/products/humidity-temperature/download-center/

  uint8_t crc = 0xff;
  uint8_t byteCtr;
  for (byteCtr = 0; byteCtr < len; ++byteCtr) {
    crc ^= (data[byteCtr]);
    for (uint8_t bit = 8; bit > 0; --bit) {
      if (crc & 0x80) {
        crc = (crc << 1) ^ 0x31;
      } else {
        crc = (crc << 1);
      }
    }
  }
  return crc;
}