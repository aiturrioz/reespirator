/** Calculation functions
 *
 * @file calc.cpp
 *
 *
 */

#include "calc.h"

/**
 * @brief Estimates tidal volume, in ml, from human height,
 * in cm, and sex.
 *
 * @param height patient height in cm
 * @param sex 0: male, 1: female
 * @return *Tidal volume (ml)
 *
 * @warning This function is unused in the code, but it is kept
 * for a future estimation functionality.
 */
int estimateTidalVolume(int height, int sex)
{
  float weight0, idealWeight;
  if (sex == 0) { // Male
    weight0 = 50.0;
  } else if (sex == 1) { // Female
    weight0 = 45.5;
  }
  idealWeight = weight0 + 0.91 * (height - 152.4); // kg


  /*if (sex == 0) { // Male
    weight0 = 23.0;
  } else if (sex == 1) { // Female
    weight0 = 21.5;
  }
  idealWeight = sqrt(height) x weight0;// kg     */

  return ((int)(round(idealWeight * DEFAULT_ML_PER_KG_IDEAL_WEIGHT)));
}
