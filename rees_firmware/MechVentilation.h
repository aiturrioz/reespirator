/** Mechanical ventilation.
 *
 * @file MechVentilation.h
 *
 * This is the mechanical ventilation software module.
 * It handles the mechanical ventilation control loop.
 */
#ifndef INC_MECHANICAL_VENTILATION_H
#define INC_MECHANICAL_VENTILATION_H

#include <float.h>
#include <inttypes.h>
#include "defaults.h"
#include "calc.h"
#include "Sensors.h"
#include "src/AutoPID/AutoPID.h"
#include "src/FlexyStepper/FlexyStepper.h"

/**
 * @brief Mechanical Ventilation states
 */
enum State
{
    Init_Insufflation = 1,
    State_Insufflation = 2, /**< Insufflating (PID control). */
    Init_Exsufflation = 3,
    State_Exsufflation = 4, /**< Return to position 0 and wait for the patient to exsufflate. */
    State_Homing = 0,
    State_Error = -1
};

/**
 * @brief Mechanical Ventilation modes
 */
enum VentilationMode
{
    Controlled_Mode = 0,
    Assisted_Mode = 1,
    Recruitment_Mode = 2
};

/**
 * @brief Mechanical Ventilation control
 */
enum VentilationControl
{
    Pressure_Control = 0,
    Volume_Control = 1
};

/**
 * @brief Mechanical Ventilation configuration
 */
typedef struct
{
  float pip;
  unsigned short timeoutIns;
  VentilationMode mode;
} Configuration_t;

/**
 * This is the mechanical ventilation class.
 */
class MechVentilation
{
public:
    MechVentilation(
        FlexyStepper *stepper,
        Sensors *sensors,
        AutoPID *pid,
        VentilationOptions_t options);

    boolean getStartWasTriggeredByPatient(void);
    void setVentilationCyle_WaitTime(float speedExsufflation);
    /** Start mechanical ventilation. */
    void start(void);
    /** Stop mechanical ventilation. */
    void stop(void);
    /** Open valves on max pressure */
    void evaluatePressure(void);
    /** Update mechanical ventilation.
     *
     * If any control variable were to change, new value
     * would be applied at the beginning of the next ventilation
     * cycle.
     *
     * @note This method must be called on a timer loop.
     */
    void update(void);

    /** Recruitment */
    void activateRecruitment(void);
    void deactivateRecruitment(void);

    /**
     * getters
     */
    bool               getSensorErrorDetected     (void);
    uint8_t            getRPM                     (void);
    short              getExsuflationTime         (void);
    short              getInsuflationTime         (void);
    short              getPeakInspiratoryPressure (void);
    short              getPeakEspiratoryPressure  (void);
    State              getState                   (void);
    int                getTriggerThreshold        (void);
    int                getIE                      (void);
    int                getFlow                    (void);
    int                getTidalVolume             (void);
    VentilationMode    getMode                    (void);
    VentilationControl getControl                 (void);
    bool               getStartStop               (void);

    /**
     * setters
     */
    void setRPM                     (uint8_t rpm);
    void setPeakInspiratoryPressure (float pip);
    void setPeakEspiratoryPressure  (float peep);
    void setState                   (State state);
    void setMode                    (VentilationMode mode);
    void setControl                 (VentilationControl control);
    void setTriggerThreshold        (int triggerThreshold);
    void setIE                      (int ie);
    void setFlow                    (int flow);
    void setTidalVolume             (int tidalVolume);

private:
    /** Initialization. */
    void _init(
        FlexyStepper *stepper,
        Sensors *sensors,
        AutoPID *pid,
        VentilationOptions_t options
    );
#if 0
    int _calculateInsuflationPosition (void);
    void _increaseInsuflationSpeed (byte factor);
    void _decreaseInsuflationSpeed (byte factor);
    void _increaseInsuflation (byte factor);
    void _decreaseInsuflation (byte factor);
#endif
    void _setInspiratoryCycle(void);

    /* Configuration parameters */
    FlexyStepper *_stepper;
    Sensors *_sensors;
    AutoPID *_pid;

    /**  Insufflation timeout in seconds. */
    unsigned short volatile _timeoutIns;
    /** Exsufflation timeout in seconds. */
    unsigned short _timeoutEsp;
    /** Breaths per minute */
    uint8_t _rpm;
    /** Peak inspiratory pressure */
    short volatile _pip;
    /** Peak espiratory pressure */
    short _peep;
    /** Inspiratory fraction */
    int _ie;
    /** Target flow, lpm */
    int _targetFlow = 30;
    /** Target tidal volume, ml */
    int _targetTidalVolume = 240;

    /* Configuration */
    volatile Configuration_t _nominalConfiguration;

    /* Internal state */
    /** Current state. */
    volatile State _currentState = State_Homing;
    volatile VentilationMode _currentMode = Controlled_Mode;
    VentilationControl _currentControl = Pressure_Control;

    /** Flow trigger value in litres per minute. */
    int _triggerThreshold = DEFAULT_TRIGGER_THRESHOLD;
    volatile bool _triggerRaised = false;

    /** Stepper speed. Steps per seconds. */
    volatile float _stepperSpeed;
    bool _running = false;
    bool _sensor_error_detected;
    volatile float _currentPressure = 0.0;
    volatile float _currentFlow = 0.0;
    volatile float _currentVolume = 0.0;
};

#endif /* INC_MECHANICAL_VENTILATION_H */
